<?php

namespace App\Controllers;

use App\core\CoreController;
use App\Functions;
use App\Models\Omdb;

class mainController extends CoreController
{

    /**
     * Display Main Page
     * @return false|string
     * @throws \Exception
     */
    public function actionIndex()
    {
        $searchText = trim($_GET['searchText']);
        if (!empty($searchText)) {
            $omdb = new Omdb();
            $data = $omdb->search($searchText);
            if ($data->Response === "True") {
                return $this->render('index', ['data' => $data]);
            } else {
                Functions::setMessage('layout-message',"Try refining your query. Remember that the name should be in English.", 'danger');
            }
        }
        return $this->render('index');
    }

    /**
     * Lazy load by ajax
     */
    public function actionGetCinema() {
        if (isset($_POST['page']) && Functions::isAjax()) {
            $searchText = trim($_GET['searchText']);
            $page = (int)$_POST['page'];
            $omdb = new Omdb();
            $data = $omdb->search($searchText, $page);
            if ($data->Response === "True") {
                $template = $this->renderPartial('cinema-templates',['data' => $data]);
                echo json_encode(['result' => true, 'template' => $template]);
            } else {
                echo json_encode(['result' => false, 'end' => true]);
            }
        }
    }

    /**
     * View information about the movie
     */
    public function actionViewInfo() {
        $id = trim($_GET['id']);
        if (!empty($id)) {
            $omdb = new Omdb();
            $data = $omdb->searchById($id);
            if ($data->Response === "True") {
                return $this->render('cinema', ['data' => $data]);
            }
        }
        return $this->redirect('index');
    }

}