<?php

namespace App;
use Exception;

trait Functions
{
    /**
     * Set message to session
     * @param $name
     * @param $message
     * @param $type
     */
    public static function setMessage($name, $message, $type) {
        session_start();
        $_SESSION['messages'][$name] = ['message' => $message, 'type' => $type];
        session_write_close();
    }

    /**
     * return and remove message from session
     * @param $name
     * @return string
     */
    public static function showMessage($name) {
        if (!empty($_SESSION['messages'][$name])) {
            session_start();
            $message = "<div class=\"alert alert-warning alert-{$_SESSION['messages'][$name]['type']} alert-dismissible fade show mx-auto\" role=\"alert\">
                            {$_SESSION['messages'][$name]['message']}
                          <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">
                            <span aria-hidden=\"true\">&times;</span>
                          </button>
                        </div>";
            unset($_SESSION['messages'][$name]);
            session_write_close();
            return $message;
        }
    }


    /**
     * @param string $url
     * @param array $params
     * @return string
     * @throws Exception
     */
    public static function goUrl(string $url, array $params = null)
    {
        if (!empty($params)) {
            $url .= "?". http_build_query($params);
        }
        return self::getBaseUrl() . "/" . $url;
    }

    /**
     * @return string
     * @throws Exception
     */
    public static function getBaseUrl()
    {
        if (isset($_scriptFile)) {
            $scriptFile = $_scriptFile;
        } elseif (isset($_SERVER['SCRIPT_FILENAME'])) {
            $scriptFile = $_SERVER['SCRIPT_FILENAME'];
        } else {
            throw new Exception('Unable to determine the entry script file path.');
        }
        $scriptName = basename($scriptFile);
        if (isset($_SERVER['SCRIPT_NAME']) && basename($_SERVER['SCRIPT_NAME']) === $scriptName) {
            $_scriptUrl = $_SERVER['SCRIPT_NAME'];
        } elseif (isset($_SERVER['PHP_SELF']) && basename($_SERVER['PHP_SELF']) === $scriptName) {
            $_scriptUrl = $_SERVER['PHP_SELF'];
        } elseif (isset($_SERVER['ORIG_SCRIPT_NAME']) && basename($_SERVER['ORIG_SCRIPT_NAME']) === $scriptName) {
            $_scriptUrl = $_SERVER['ORIG_SCRIPT_NAME'];
        } elseif (isset($_SERVER['PHP_SELF']) && ($pos = strpos($_SERVER['PHP_SELF'], '/' . $scriptName)) !== false) {
            $_scriptUrl = substr($_SERVER['SCRIPT_NAME'], 0, $pos) . '/' . $scriptName;
        } elseif (!empty($_SERVER['DOCUMENT_ROOT']) && strpos($scriptFile, $_SERVER['DOCUMENT_ROOT']) === 0) {
            $_scriptUrl = str_replace('\\', '/', str_replace($_SERVER['DOCUMENT_ROOT'], '', $scriptFile));
        } else {
            throw new Exception('Unable to determine the entry script URL.');
        }
        $_baseUrl = rtrim(dirname($_scriptUrl), '\\/');

        return $_baseUrl;
    }


    /** Проверка запроса на ajax
     * @return bool
     */
    public static function isAjax()
    {
        return (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
            && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
        );
    }
}

?>