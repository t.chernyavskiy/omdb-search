<?php

namespace App\Models;

use App\core\App;

class Omdb {

    //API url
    private $url = 'http://www.omdbapi.com/';
    //Request timeout
    private $timeout;
    //Date format
    private $date_format;
    //ApiKey
    private $apiKey;

    //Default parameters
    private $params = [
        //A valid IMDb ID (e.g. tt1285016)
        'i' => null,
        //Movie title to search for.
        't' => null,
        //movie, series, episode or null
        'type' => null,
        //Year of release or null
        'y' => null,
        //short, full
        'plot' => 'full',
        //	The data type to return. json, xml
        'r' => 'json',
        //	JSONP callback name.
        'callback' => null,
        //Movie title to search for.
        's' => null,
        //Page number to return. (new)
        'page' => 1,
        //API version
        'v' => null
    ];

    public function __construct($timeout = 5, $date_format = 'd.m.Y') {
        $this->apiKey = App::getConfigEnv('apiKey');
        //Set the cURL timeout
        $this->timeout = $timeout;
        //Set the date format
        $this->date_format = $date_format;
    }

    /**
     * Set the parameters for the API request
     * @param $param
     * @param $value
     * @throws \Exception
     */
    public function setParam($param, $value) {

        if (array_key_exists($param, $this->params)) {
            $this->params[$param] = $value;
        } else {
            throw new \Exception("The parameter is not found");
        }
    }

    /**
     * Create URL for request
     * @return string
     */
    private function createURL() {
        $params = ['apikey' => $this->apiKey];
        foreach($this->params as $param => $value) {
            if (is_null($value)) {
                continue;
            }
            //Bool to string
            if(is_bool($value)) {
                $value = ($value) ? 'true' : 'false';
            }
            $params[$param] = $value;
        }
        $query = http_build_query($params);
        $this->url .= '?' . $query;
    }

    /**
     * Create request on server and return response
     * @return mixed
     */
    private function request() {
        $this->createURL();
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->url);
        curl_setopt($ch, CURLOPT_TIMEOUT, $this->timeout);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $content = curl_exec($ch);
        $info = curl_getinfo($ch);
        //Checks if the request did succed
        if($info['http_code'] !== 200) {
            throw new Exception(
                'Request failed. HTTP CODE: '
                . $info['http_code']
            );
        }
        return json_decode($content);
    }

    public function search($text, $page = null) {
        $this->setParam('s', $text);
        if (!is_null($page)) {
            $this->setParam('page', $page);
        }
//        $this->setParam('type', 'movie');
        return $this->request();
    }

    public function searchById($id) {
        $this->setParam('i', $id);
        return $this->request();
    }
}