<?php

use App\Functions;

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="<?= Functions::getBaseUrl() ?>/css/bootstrap.min.css">
    <title>Page not found</title>
    <style>
        .errPage {
            max-width: 500px;
            margin: auto;
        }
    </style>
</head>
<body>
<div class="errPage">
    <a href="<?=Functions::goUrl('index')?>" title="Отведи меня домой"><img src="<?= Functions::getBaseUrl()?>/img/404.png"  class="img-fluid" alt="404"></a>
</div>
</body>
</html>