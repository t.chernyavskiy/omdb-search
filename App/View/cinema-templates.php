<?php

use App\Functions;

foreach ($data->Search as $movie) {
    ?>
    <!-- Card Narrower -->
    <div class="card card-cascade narrower col-md-6 col-sm-6 cinema">

        <!-- Card image -->
        <div class="view view-cascade overlay cinema-poster"
             style="<?=($movie->Poster != "N/A") ? "background-image: url(".$movie->Poster.")" : ""?>">
<!--            <img  class="card-img-top" src="" alt="Poster image">-->
            <a href="<?= Functions::goUrl('main/viewInfo',['id' => $movie->imdbID])?>">
                <div class="mask rgba-white-slight"></div>
            </a>
        </div>

        <!-- Card content -->
        <div class="card-body card-body-cascade">

            <!-- Label -->
            <h5 class="pink-text pb-2 pt-1"><?=$movie->Year?></h5>
            <!-- Title -->
            <h4 class="font-weight-bold card-title"><?=$movie->Title?></h4>
            <!-- Text -->
            <p class="card-text"><?=$movie->Type?></p>
            <!-- Button -->
            <a href="<?= Functions::goUrl('main/viewInfo',['id' => $movie->imdbID])?>" class="btn btn-primary">More</a>

        </div>

    </div>
    <!-- Card Narrower -->
<?php } ?>