<?php

use App\Functions; ?>
<div class="container">
    <div class="row">
        <div class="w-100">
            <a href="<?=$_SERVER['HTTP_REFERER']?>" class="btn btn-outline-primary btn-rounded waves-effect float-right">Back</a>
        </div>
        <div class="media">
            <img src="<?= ($data->Poster != "N/A") ? $data->Poster : Functions::getBaseUrl()."/img/no-poster.png" ?>" class="img-fluid d-flex align-self-start mr-3 z-depth-2" alt="Poster img">
            <div class="media-body">
                <h2 class="h2-responsive mt-0 font-weight-bold"><?=$data->Title?></h2>
                <p><span class="font-weight-bold">Year: </span><?=$data->Year?></p>
                <p><span class="font-weight-bold">Released: </span><?=$data->Released?></p>
                <p><span class="font-weight-bold">Runtime: </span><?=$data->Runtime?></p>
                <p><span class="font-weight-bold">Genre: </span><?=$data->Genre?></p>
                <p><span class="font-weight-bold">Actors: </span><?=$data->Actors?></p>
                <p><span class="font-weight-bold">Plot: </span><?=$data->Plot?></p>
                <p><span class="font-weight-bold">Awards: </span><?=$data->Awards?></p>
                <p><span class="font-weight-bold">Metascore: </span><?=$data->Metascore?></p>
                <p><span class="font-weight-bold">imdbRating: </span><?=$data->imdbRating?></p>
                <p><span class="font-weight-bold">Type: </span><?=$data->Type?></p>
                <p><span class="font-weight-bold">DVD: </span><?=$data->DVD?></p>
                <p><span class="font-weight-bold">BoxOffice: </span><?=$data->BoxOffice?></p>
                <p><span class="font-weight-bold">Production: </span><?=$data->Production?></p>
                <p><span class="font-weight-bold">Website: </span><?=($data->Website != "N/A") ? "<a href=\"{$data->Website}\" target=\"_blank\">{$data->Website}</a>" : ""?></p>
            </div>
        </div>
    </div>
</div>
