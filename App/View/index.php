<?php

$this->title = "Search Films and Series";
?>
<div class="container">
    <div class="row cinema-library">
        <?php
            if (isset($data) && !empty($data)) {
                echo $this->renderPartial('cinema-templates',['data' => $data]);
            } else {
        ?>
                <div class="col-12 text-center">
                    <h1 class="animated fadeIn mb-3 h1-responsive">Thank you for using our film and series search.</h1>
                </div>
                <div class="mx-auto">
            <img class="img-fluid img-responsive animated fadeIn main-page-img" src="<?=\App\Functions::getBaseUrl()?>/img/mainpage.png" alt="main page image">
        </div>
        <?php } ?>
    </div>
</div>
