<?php

use App\Functions;

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title><?=$this->title ?></title>
    <link rel="shortcut icon" href="<?= Functions::getBaseUrl() ?>/favicon.png" type="image/x-icon">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css">
    <!-- Bootstrap core CSS -->
    <link href="<?=Functions::getBaseUrl()?>/css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="<?=Functions::getBaseUrl()?>/css/mdb.min.css" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="<?=Functions::getBaseUrl()?>/css/style.css" rel="stylesheet">
</head>

<body>
<!--Navbar-->
<nav class="navbar navbar-expand-sm navbar-dark indigo mb-4">

    <!-- Navbar brand -->
    <a class="navbar-brand font-weight-bold text-uppercase" href="<?=Functions::goUrl('index')?>"><i class="fas fa-film"></i> Cinema library</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="true" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
    <!-- Collapsible content -->

    <!-- Collapsible content -->
    <div class="collapse navbar-collapse" id="navbarSupportedContent">

        <form class="form-inline ml-auto" method="get" action="<?=Functions::goUrl('index')?>">
            <div class="md-form my-0">
                <input class="form-control" type="text" name="searchText" value="<?=$_GET['searchText']?>" placeholder="Search film or series" aria-label="Search" required>
            </div>
            <button href="#!" class="btn btn-outline-white btn-md my-0 ml-sm-2" type="submit">Search</button>
        </form>

    </div>

</nav>
<!-- Start your project here-->
<div>
    <div class="container">
        <div class="row">
            <?=Functions::showMessage('layout-message')?>
        </div>
    </div>
    <?=$content?>

    <button class="btn btn-primary to-top"><i class="fas fa-arrow-circle-up"></i></button>
    <div class="cssload-jumping ajaxLoader">
        <span></span><span></span><span></span><span></span><span></span>
    </div>

</div>
<!-- /Start your project here-->

<!-- SCRIPTS -->
<!-- JQuery -->
<script type="text/javascript" src="<?=Functions::getBaseUrl()?>/js/jquery-3.3.1.min.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="<?=Functions::getBaseUrl()?>/js/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="<?=Functions::getBaseUrl()?>/js/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="<?=Functions::getBaseUrl()?>/js/mdb.js"></script>

<script type="text/javascript" src="<?=Functions::getBaseUrl()?>/js/site.js"></script>
</body>

</html>