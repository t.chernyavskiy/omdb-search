<?php
namespace App\core;

use App\Controllers\mainController;
use App\Functions;
use mysql_xdevapi\Exception;

class App
{
    private static $config;

    public function __construct($config = null)
 {
     self::$config = $config;
 }

    /**
     * init script application
     * @throws \Exception
     */
    public function init()
 {

     $url = $_SERVER['REQUEST_URI'];

     /**
      * Перехват ajax запросов для перенаправления в "controller/action"
      */
     if (Functions::isAjax()) {
         $url = explode('/', $url);
         $url[1] = $url[count($url) - 2];
         $url[2] = $url[count($url) - 1];
     } else {
         $url = str_replace(Functions::getBaseUrl(), '', $url);
         $url = explode('/', $url);
     }

     $class = (!empty($url[1])) ? "\\App\\Controllers\\" . $url[1] . "Controller" : "\\App\\Controllers\\mainController";
     if (class_exists($class)) {
         $controller = new $class;
         $action = (empty($url[2])) ? 'Index' : $url[2];
         $controller->beforeAction($action);
     } else {
         $controller = new mainController();
         $action = (empty($url[2])) ? 'Index' : $url[2];
         $controller->beforeAction($action);
     }
 }

    /**
     * set parameter to application config
     * @param $name
     * @param $value
     * @throws \Exception
     */
    public function setConfigEnv($name, $value) {
        if (array_key_exists($name, self::$config)) {
            throw new \Exception("Parameter already exists");
        } else {
            self::$config[$name] = $value;
        }
    }

    /**
     * get parameter from application config
     * @param $name
     * @return mixed
     * @throws \Exception
     */
    public static function getConfigEnv($name) {
        if (array_key_exists($name, self::$config)) {
            return self::$config[$name];
        } else {
            throw new \Exception("The parameter is not found");
        }
    }
}