<?php

namespace App\core;

abstract class CoreController
{
    public $action;

    public $title;

    protected $layout = "layout/main";

    protected function render($page, $args = [])
    {
        $content = $this->renderFile($page, $args);
        return $this->renderFile($this->layout, ['content' => $content]);
    }

    protected function redirect($url, $params = null)
    {
        if (!empty($params)) {
            $url .= "?". http_build_query($params);
        }
        header("location:" . $url);
    }

    public function renderPartial($page, $args = [])
    {
        return $this->renderFile($page, $args);
    }

    public function beforeAction($action)
    {
        $this->action = explode("?",$action)[0];
        $action = 'action' . $this->action;
        $render = (method_exists($this, $action)) ? $this->$action() : $this->renderPartial('404');
        $this->afterAction($render);
    }

    public function isActive($action) {
        return ($this->action == $action) ? "active" : "";
    }

    protected function afterAction($actionResult) {
        echo $actionResult;
    }

    public abstract function actionIndex();

    protected function setLayout($layout)
    {
        $this->layout = $layout;
    }

    private function renderFile($page, $args = []) {
        $file = __DIR__ . '/../View/' . $page . '.php';
        if (file_exists($file)) {
            ob_start();
            extract($args, EXTR_OVERWRITE);
            require $file;
            return ob_get_clean();
        } else {
            throw new \Exception('File not exist: ' . $file);
        }
    }

}