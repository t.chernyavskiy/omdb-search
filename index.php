<?php

use App\core\App;

require_once __DIR__ . '/autoload.php';

$config = require_once __DIR__ . '/App/config.php';

$app = new App($config);

$app->init();