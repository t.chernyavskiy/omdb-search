$(document).scroll(function () {
    if ($(this).scrollTop() > 100) {
        $('.to-top').show();
    } else {
        $('.to-top').hide();
    }
});

$('.to-top').on('click',function () {
    $('html, body').animate({scrollTop: 0}, 600);
});

$(document).ready(function(){
    var inProgress = false;
    var startFrom = 2;
    var end = false;
    var blockHeight = parseInt($('.container .cinema:last').css("height"));
    $(window).scroll(function() {
        if(!inProgress && !end && ($(window).scrollTop() + $(window).height() >= $(document).height() - (blockHeight+250))) {
            inProgress = true;
            $.ajax({
                url: 'main/GetCinema' + location.search,
                method: 'POST',
                dataType: 'json',
                data: {"page" : startFrom},
                beforeSend: function() {
                    $('.ajaxLoader').show();
                }
            })
                .done(function(data){
                    console.log(data);
                    if (data.result) {
                        $('.cinema-library').append(data.template);
                        inProgress = false;
                        startFrom++;
                    }
                    else if (data.end) {
                        end = true;
                    }
                    else {
                        inProgress = false;
                    }
                    $('.ajaxLoader').hide();
                });
        }
    });
});


